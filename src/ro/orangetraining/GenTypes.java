package ro.orangetraining;

public class GenTypes <T1,T2> {
    private T1 nume;
    private T2 salariu;

    public GenTypes(T1 nume,T2 salariu){
        this.nume=nume;
        this.salariu=salariu;

    }

    public T1 getNume() {
        return nume;
    }

    public T2 getSalariu() {
        return salariu;
    }
}
